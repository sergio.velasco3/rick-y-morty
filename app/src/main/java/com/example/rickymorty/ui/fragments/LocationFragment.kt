package com.example.rickymorty.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.rickymorty.R
import com.example.rickymorty.data.models.Personaje
import com.example.rickymorty.data.network.ApiResult
import com.example.rickymorty.databinding.FragmentLocationBinding
import com.example.rickymorty.ui.MainActivity
import com.example.rickymorty.ui.MyViewModel
import com.example.rickymorty.ui.adapters.PersonajeAdapter

class LocationFragment : Fragment() {

    private lateinit var adapter: PersonajeAdapter
    private lateinit var binding: FragmentLocationBinding
    private val myViewModel: MyViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLocationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            if (it.containsKey("url")) {
                val url = it.getString("url")
                if (url != null) {
                    getLocationByUrl(url)
                }
            }

            if (it.containsKey("id")) {
                val id = it.getString("id")
                if (id != null) {
                    getLocationById(id)
                }
            }
        }
    }

    fun getLocationById(id: String) {
        myViewModel.getLocationById(id.toInt()).observe(viewLifecycleOwner) {
            binding.progressBar2.visibility = View.VISIBLE
            when (it) {
                is ApiResult.ApiSuccess -> {
                    (requireActivity() as MainActivity).supportActionBar?.title = it.data.name
                    binding.tvName.text = it.data.name
                    binding.tvType.text = it.data.type
                    binding.tvDimension.text = it.data.dimension

                    configRecyclerView(it.data.residents)
                }
                is ApiResult.ApiError -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                }

                is ApiResult.ApiException -> {
                    Toast.makeText(requireContext(), it.e.toString(), Toast.LENGTH_SHORT).show()
                }

                is ApiResult.ApiLoading -> {
                    binding.progressBar2.visibility = View.VISIBLE
                }
            }

        }
    }

    fun getLocationByUrl(url: String) {
        myViewModel.getLocationByUrl(url).observe(viewLifecycleOwner) {
            binding.progressBar2.visibility = View.VISIBLE
            when (it) {
                is ApiResult.ApiSuccess -> {
                    (requireActivity() as MainActivity).supportActionBar?.title = it.data.name
                    binding.tvName.text = it.data.name
                    binding.tvType.text = it.data.type
                    binding.tvDimension.text = it.data.dimension

                    configRecyclerView(it.data.residents)
                }
                is ApiResult.ApiError -> {
                       Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                }

                is ApiResult.ApiException -> {
                    Toast.makeText(requireContext(), it.e.toString(), Toast.LENGTH_SHORT).show()
                }

                is ApiResult.ApiLoading -> {
                    binding.progressBar2.visibility = View.VISIBLE
                }
            }

        }
    }

    fun configRecyclerView(residents: List<String?>?) {

        adapter = PersonajeAdapter(object : PersonajeAdapter.OnItemClickListener {
            override fun onItemClick(personaje: Personaje) {
                myViewModel.selectedPersonaje.value = personaje
                findNavController().navigate(R.id.action_locationFragment_to_fragmentPersonaje)
            }
        })
        val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        binding.recyclerview.layoutManager = layoutManager
        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        binding.recyclerview.adapter = adapter

        val idsList = ArrayList<String>()
        residents?.forEach { url ->
            val id = url?.substringAfterLast("/")
            if (id != null) {
                idsList.add(id)
            }
        }

        myViewModel.getPersonajesByIds(idsList.toString()).observe(viewLifecycleOwner) {
            binding.progressBar2.visibility = View.INVISIBLE
            when(it) {
                is ApiResult.ApiSuccess -> adapter.updateList(it.data)
                is ApiResult.ApiError -> {
                    when (it.code) {
                        401 -> Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        402 -> Toast.makeText(requireContext(), "Está mal la ID", Toast.LENGTH_SHORT).show()
                        403 -> Toast.makeText(requireContext(), "Otra cosa", Toast.LENGTH_SHORT).show()
                    }
                }

                is ApiResult.ApiException -> {
                    Toast.makeText(requireContext(), it.e.toString(), Toast.LENGTH_SHORT).show()
                }

                is ApiResult.ApiLoading -> {
                    binding.progressBar2.visibility = View.VISIBLE
                }
            }

        }

    }
}