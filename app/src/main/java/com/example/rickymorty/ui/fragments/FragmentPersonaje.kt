package com.example.rickymorty.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.rickymorty.R
import com.example.rickymorty.databinding.PersonajeFragmentBinding
import com.example.rickymorty.ui.MainActivity
import com.example.rickymorty.ui.MyViewModel
import com.example.rickymorty.ui.adapters.ChaptersAdapter


/**
 * Created by sergi on 05/04/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class FragmentPersonaje : Fragment() {

    private lateinit var binding: PersonajeFragmentBinding
    private val myViewModel: MyViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PersonajeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myViewModel.selectedPersonaje.observe(viewLifecycleOwner) {
            (requireActivity() as MainActivity).supportActionBar?.title = it.name
            binding.tvName.text = it.name
            Glide.with(this).load(it.image).into(binding.ivImage)

            binding.recyclerview.layoutManager = LinearLayoutManager(requireContext())
            binding.recyclerview.adapter =
                ChaptersAdapter(it.episode, object : ChaptersAdapter.OnItemClickListener {
                    override fun itemClick(url: String) {
                        val id = url.substringAfterLast("/")
                        val bundle = bundleOf(
                            "id" to id
                        )
                        findNavController().navigate(
                            R.id.action_fragmentPersonaje_to_episodeFragment,
                            bundle
                        )
                    }
                })

            binding.tvStatus.text = it.status
            binding.tvSpecies.text = it.species
            binding.tvOrigin.text = it.origin.name
            binding.tvLocation.text = it.location.name
            binding.tvGender.text = it.gender

            binding.butOri.isVisible = !it.origin.url.isEmpty()
            binding.butLoc.isVisible = !(it.location.url?.isEmpty())!!

            binding.butLoc.setOnClickListener { it1 ->
                if (!it.location.url.isNullOrEmpty()) {
                    val bundle = bundleOf(
                        "url" to it.location.url
                    )
                    findNavController().navigate(
                        R.id.action_fragmentPersonaje_to_locationFragment,
                        bundle
                    )
                }
            }

            binding.butOri.setOnClickListener { it1 ->
                val id = it.origin.url.substringAfterLast("/")
                val bundle = bundleOf(
                    "id" to id
                )
                findNavController().navigate(
                    R.id.action_fragmentPersonaje_to_locationFragment,
                    bundle
                )
            }
        }

    }
}