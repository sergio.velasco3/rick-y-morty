package com.example.rickymorty.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.rickymorty.R
import com.example.rickymorty.data.models.Personaje
import com.example.rickymorty.data.network.ApiResult
import com.example.rickymorty.databinding.FragmentEpisodeBinding
import com.example.rickymorty.ui.MainActivity
import com.example.rickymorty.ui.MyViewModel
import com.example.rickymorty.ui.adapters.PersonajeAdapter

class EpisodeFragment : Fragment() {

    private lateinit var adapter: PersonajeAdapter
    private lateinit var binding: FragmentEpisodeBinding
    private val myViewModel: MyViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEpisodeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        arguments?.let {
            if (it.containsKey("id")) {
                val id = it.getString("id")
                if (id != null) {
                    getEpisodeById(id)
                }
            }
        }
    }

    fun getEpisodeById(id: String) {
        myViewModel.getEpisodeById(id.toInt()).observe(viewLifecycleOwner) {
            binding.carga.visibility = View.INVISIBLE
            when (it) {
                is ApiResult.ApiSuccess -> {
                    val episode = it.data
                    (requireActivity() as MainActivity).supportActionBar?.title = episode.name
                    binding.tvName.text = episode.name
                    binding.tvDay.text = episode.airDate
                    binding.tvEpisode.text = episode.episode

                    configRecyclerView(episode.characters)
                }

                is ApiResult.ApiError -> {
                    binding.carga.visibility = View.INVISIBLE
                    when (it.code) {
                        401 -> Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        402 -> Toast.makeText(requireContext(), "Está mal la ID", Toast.LENGTH_SHORT).show()
                        403 -> Toast.makeText(requireContext(), "Otra cosa", Toast.LENGTH_SHORT).show()
                    }
                }

                is ApiResult.ApiException -> {
                    binding.carga.visibility = View.INVISIBLE
                    Toast.makeText(requireContext(), it.e.toString(), Toast.LENGTH_SHORT).show()
                }

                is ApiResult.ApiLoading -> {
                    binding.carga.visibility = View.VISIBLE
                }
            }
        }
    }

    fun configRecyclerView(residents: List<String?>?) {

        adapter = PersonajeAdapter(object : PersonajeAdapter.OnItemClickListener {
            override fun onItemClick(personaje: Personaje) {
                myViewModel.selectedPersonaje.value = personaje
                findNavController().navigate(R.id.action_episodeFragment_to_fragmentPersonaje)
            }
        })
        val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        binding.recyclerview.layoutManager = layoutManager
        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        binding.recyclerview.adapter = adapter

        val idsList = ArrayList<String>()
        residents?.forEach { url ->
            val id = url?.substringAfterLast("/")
            if (id != null) {
                idsList.add(id)
            }
        }

        myViewModel.getPersonajesByIds(idsList.toString()).observe(viewLifecycleOwner) {
            binding.carga.visibility = View.INVISIBLE
            when(it) {
                is ApiResult.ApiSuccess -> adapter.updateList(it.data)
                is ApiResult.ApiError -> {
                    when (it.code) {
                        401 -> Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        402 -> Toast.makeText(requireContext(), "Está mal la ID", Toast.LENGTH_SHORT).show()
                        403 -> Toast.makeText(requireContext(), "Otra cosa", Toast.LENGTH_SHORT).show()
                    }
                }

                is ApiResult.ApiException -> {
                    Toast.makeText(requireContext(), it.e.toString(), Toast.LENGTH_SHORT).show()
                }

                is ApiResult.ApiLoading -> {
                    binding.carga.visibility = View.VISIBLE
                }
            }

        }
    }
}