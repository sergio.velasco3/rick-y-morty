package com.example.rickymorty.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.estech.retrofitsample.models.Info
import com.estech.retrofitsample.models.Respuesta
import com.example.rickymorty.data.Repositorio
import com.example.rickymorty.data.models.Episode
import com.example.rickymorty.data.models.Location
import com.example.rickymorty.data.models.Personaje
import com.example.rickymorty.data.network.ApiResult
import kotlinx.coroutines.launch

class MyViewModel : ViewModel() {

    private val repositorio = Repositorio()

    val personajesLiveData = MutableLiveData<ApiResult<Respuesta>>()
    val selectedPersonaje = MutableLiveData<Personaje>()

    fun getPersonajes() {
        personajesLiveData.postValue(ApiResult.ApiLoading)

        viewModelScope.launch {
            val response = repositorio.getPersonajes()

            try {
                if (response.code() == 200) {
                    val miRespuesta = response.body()
                    miRespuesta?.let { personajesLiveData.postValue(ApiResult.ApiSuccess(it)) }
                } else {
                    personajesLiveData.postValue(
                        ApiResult.ApiError(
                            response.code(),
                            response.message()
                        )
                    )
                }
            } catch (e: Exception) {
                personajesLiveData.postValue(ApiResult.ApiException(e))
            }
        }
    }

    fun buscarPorNombre(nombre: String, page: Int) {

        personajesLiveData.postValue(ApiResult.ApiLoading)

        viewModelScope.launch {
            val response = repositorio.buscarPorNombre(nombre, page)

            try {
                if (response.code() == 200) {
                    val miRespuesta = response.body()
                    miRespuesta?.let { personajesLiveData.postValue(ApiResult.ApiSuccess(it)) }
                } else {
                    personajesLiveData.postValue(
                        ApiResult.ApiError(
                            response.code(),
                            response.message()
                        )
                    )
                }
            } catch (e: Exception) {
                personajesLiveData.postValue(ApiResult.ApiException(e))
            }
        }
    }

    fun buscarPorPagina(page: Int) {
        personajesLiveData.postValue(ApiResult.ApiLoading)

        viewModelScope.launch {
            val response = repositorio.buscarPorPagina(page)

            try {
                if (response.code() == 200) {
                    val miRespuesta = response.body()
                    miRespuesta?.let { personajesLiveData.postValue(ApiResult.ApiSuccess(it)) }
                } else {
                    personajesLiveData.postValue(
                        ApiResult.ApiError(
                            response.code(),
                            response.message()
                        )
                    )
                }
            } catch (e: Exception) {
                personajesLiveData.postValue(ApiResult.ApiException(e))
            }
        }
    }

    fun getLocationByUrl(url: String): MutableLiveData<ApiResult<Location>> {
        val locationLiveData = MutableLiveData<ApiResult<Location>>()

        locationLiveData.postValue(ApiResult.ApiLoading)

        viewModelScope.launch {
            val response = repositorio.getLocationByUrl(url)

            try {
                if (response.code() == 200) {
                    val miRespuesta = response.body()
                    miRespuesta?.let { locationLiveData.postValue(ApiResult.ApiSuccess(it)) }
                } else {
                    locationLiveData.postValue(
                        ApiResult.ApiError(
                            response.code(),
                            response.message()
                        )
                    )
                }
            } catch (e: Exception) {
                locationLiveData.postValue(ApiResult.ApiException(e))
            }
        }
        return locationLiveData
    }

    fun getLocationById(id: Int): MutableLiveData<ApiResult<Location>> {
        val locationLiveData = MutableLiveData<ApiResult<Location>>()

        locationLiveData.postValue(ApiResult.ApiLoading)

        viewModelScope.launch {
            val response = repositorio.getLocationById(id)

            try {
                if (response.code() == 200) {
                    val miRespuesta = response.body()
                    miRespuesta?.let { locationLiveData.postValue(ApiResult.ApiSuccess(it)) }
                } else {
                    locationLiveData.postValue(
                        ApiResult.ApiError(
                            response.code(),
                            response.message()
                        )
                    )
                }
            } catch (e: Exception) {
                locationLiveData.postValue(ApiResult.ApiException(e))
            }
        }
        return locationLiveData
    }

    fun getEpisodeById(id: Int): MutableLiveData<ApiResult<Episode>> {
        val locationLiveData = MutableLiveData<ApiResult<Episode>>()
        locationLiveData.postValue(ApiResult.ApiLoading)

        viewModelScope.launch {
            val response = repositorio.getEpisodiosByIds(id)

            try {
                if (response.code() == 200) {
                    val miRespuesta = response.body()
                    miRespuesta?.let { locationLiveData.postValue(ApiResult.ApiSuccess(it)) }
                } else {
                    locationLiveData.postValue(
                        ApiResult.ApiError(
                            response.code(),
                            response.message()
                        )
                    )
                }
            } catch (e: Exception) {
                locationLiveData.postValue(ApiResult.ApiException(e))
            }
        }
        return locationLiveData
    }

    fun getPersonajesByIds(ids: String): MutableLiveData<ApiResult<List<Personaje>>> {
        val locationLiveData = MutableLiveData<ApiResult<List<Personaje>>>()
        locationLiveData.postValue(ApiResult.ApiLoading)

        viewModelScope.launch {
            val response = repositorio.getPersonajesByIds(ids)

            try {
                if (response.code() == 200) {
                    val miRespuesta = response.body()
                    miRespuesta?.let { locationLiveData.postValue(ApiResult.ApiSuccess(it)) }
                } else {
                    locationLiveData.postValue(
                        ApiResult.ApiError(
                            response.code(),
                            response.message()
                        )
                    )
                }
            } catch (e: Exception) {
                locationLiveData.postValue(ApiResult.ApiException(e))
            }
        }
        return locationLiveData
    }
}