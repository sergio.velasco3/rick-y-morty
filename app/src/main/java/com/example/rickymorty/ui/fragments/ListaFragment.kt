package com.example.rickymorty.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.estech.retrofitsample.models.Respuesta
import com.example.rickymorty.R
import com.example.rickymorty.data.models.Personaje
import com.example.rickymorty.data.network.ApiResult
import com.example.rickymorty.databinding.ListaFragmentBinding
import com.example.rickymorty.ui.MyViewModel
import com.example.rickymorty.ui.adapters.PersonajeAdapter

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ListaFragment : Fragment() {

    private var _binding: ListaFragmentBinding? = null
    private val myViewModel by activityViewModels<MyViewModel> ()
    private var currentPage = 1
    private var totalPages = 0
    private var textToSearch = ""
    private lateinit var listAdapter: PersonajeAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = ListaFragmentBinding.inflate(inflater, container, false)
        return binding.root

    }

    private fun mostrarError(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerview.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)

        listAdapter = PersonajeAdapter(object : PersonajeAdapter.OnItemClickListener {
            override fun onItemClick(personaje: Personaje) {
                myViewModel.selectedPersonaje.value = personaje
                findNavController().navigate(R.id.action_FirstFragment_to_fragmentPersonaje)
            }
        })

        listAdapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        binding.recyclerview.adapter = listAdapter

        myViewModel.personajesLiveData.observe(viewLifecycleOwner, observerCallback)
        myViewModel.buscarPorNombre(textToSearch, currentPage)



        binding.etBuscar.addTextChangedListener {
            textToSearch = it.toString()
        }

        binding.swipe.setOnRefreshListener {
            binding.swipe.isRefreshing = true
            currentPage = 1
            binding.etBuscar.setText("")
            myViewModel.buscarPorNombre(textToSearch, currentPage)
        }

        binding.ivLeft.setOnClickListener {
            currentPage--
            myViewModel.buscarPorNombre(textToSearch, currentPage)
        }

        binding.ivRight.setOnClickListener {
            currentPage++
            myViewModel.buscarPorNombre(textToSearch, currentPage)
        }
        
        binding.butSend.setOnClickListener { 
            val name = binding.etBuscar.text.toString()
            if (name.isNotEmpty()) {
                currentPage = 1
                myViewModel.buscarPorNombre(name, currentPage)
            } else {
                Toast.makeText(requireContext(), "No has escrito nada", Toast.LENGTH_SHORT).show()
            }
        }
    }

    val observerCallback = Observer<ApiResult<Respuesta>> {
        binding.progressBar.isVisible = false
        binding.swipe.isRefreshing = false
        when(it) {
            is ApiResult.ApiSuccess -> {
                listAdapter.updateList(it.data.results)
                totalPages = it.data.info.pages
                binding.ivLeft.isVisible = it.data.info.prev != null
                binding.ivRight.isVisible = it.data.info.next != null
            }
            is ApiResult.ApiError -> mostrarError(it.message)
            is ApiResult.ApiException -> it.e.message?.let { it1 -> mostrarError(it1) }
            is ApiResult.ApiLoading -> binding.progressBar.isVisible = false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}