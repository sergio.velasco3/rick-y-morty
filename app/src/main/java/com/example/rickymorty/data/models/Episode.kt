package com.example.rickymorty.data.models


import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize
import android.os.Parcelable

@Parcelize
data class Episode(
    @field:Json(name = "air_date")
    val airDate: String,
    val characters: List<String?>?,
    val created: String?,
    val episode: String?,
    val id: Int?,
    val name: String?,
    val url: String?
) : Parcelable