package com.estech.retrofitsample.models


import android.os.Parcelable
import com.example.rickymorty.data.models.Personaje
import kotlinx.parcelize.Parcelize

@Parcelize
data class Respuesta(
    val info: Info,
    val results: List<Personaje>
) : Parcelable