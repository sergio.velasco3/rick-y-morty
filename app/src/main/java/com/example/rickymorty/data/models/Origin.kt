package com.example.rickymorty.data.models


import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize
import android.os.Parcelable

@Parcelize
data class Origin(
    val name: String,
    val url: String
) : Parcelable