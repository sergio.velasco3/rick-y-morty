package com.example.rickymorty.data

import com.example.rickymorty.data.network.BasicCall
import com.example.rickymorty.data.network.RetrofitHelper


class Repositorio : BasicCall() {

    private val retrofit = RetrofitHelper.retrofitService

    suspend fun getPersonajes() = retrofit.dameTodosPersonajes()
    suspend fun buscarPorPagina(page: Int) = retrofit.getByPage(page)
    suspend fun buscarPorNombre(name: String, page: Int) = retrofit.buscarPorNombre(name, page)

    suspend fun getLocationByUrl(url: String) = retrofit.getLocationByUrl(url)

    suspend fun getLocationById(idLocation: Int) = retrofit.getLocationById(idLocation)
    suspend fun getPersonajesByIds(ids: String) = retrofit.getPersonajesByIds(ids)

    //    suspend fun getEpisodiosByIds(idEpisode: Int) = safeApiCall { retrofit.getEpisodeById(idEpisode) }
    suspend fun getEpisodiosByIds(idEpisode: Int) = retrofit.getEpisodeById(idEpisode)

}