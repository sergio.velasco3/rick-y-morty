package com.example.rickymorty.data.models


import com.squareup.moshi.Json

data class ResponseVote(
    @Json(name = "country_code")
    val countryCode: String,
    @Json(name = "id")
    val id: Int,
    @Json(name = "image_id")
    val imageId: String,
    @Json(name = "message")
    val message: String,
    @Json(name = "sub_id")
    val subId: String,
    @Json(name = "value")
    val value: Int
)