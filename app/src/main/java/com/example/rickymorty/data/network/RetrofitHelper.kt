package com.example.rickymorty.data.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitHelper {

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://rickandmortyapi.com/api/")
        .addConverterFactory(MoshiConverterFactory.create())
        .client(OkHttpClient.Builder().addInterceptor(httplogging()).build())
        .build()

    val retrofitService by lazy {
        retrofit.create(ApiService::class.java)
    }

    private fun httplogging() = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
}