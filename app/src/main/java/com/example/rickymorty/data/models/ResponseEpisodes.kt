package com.example.rickymorty.data.models


import kotlinx.parcelize.Parcelize
import android.os.Parcelable
import com.estech.retrofitsample.models.Info

@Parcelize
data class ResponseEpisodes(
    val info: Info?,
    val results: List<Episode>?
) : Parcelable