package com.example.rickymorty.data.models


import android.os.Parcelable
import com.estech.retrofitsample.models.Info
import kotlinx.parcelize.Parcelize

@Parcelize
data class ResponseLocation(
    val info: Info?,
    val results: List<Location>?
) : Parcelable