package com.example.rickymorty.data.models


import com.squareup.moshi.Json

data class SendVote(
    @field:Json(name = "image_id")
    val imageId: String,
    @field:Json(name = "sub_id")
    val subId: String,
    @field:Json(name = "value")
    val value: Int
)