package com.example.rickymorty.data.network

import com.estech.retrofitsample.models.Respuesta
import com.example.rickymorty.data.models.Episode
import com.example.rickymorty.data.models.Location
import com.example.rickymorty.data.models.Personaje
import com.example.rickymorty.data.models.ResponseVote
import com.example.rickymorty.data.models.SendVote
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface ApiService {

    @GET("character")
    suspend fun dameTodosPersonajes(): Response<Respuesta>

    @GET("character")
    suspend fun buscarPorNombre(
        @Query("name") name: String,
        @Query("page") page: Int
    ): Response<Respuesta>

    @GET("character")
    suspend fun getByPage(
        @Query("page") page: Int
    ): Response<Respuesta>

    @GET()
    suspend fun getLocationByUrl(
        @Url url: String
    ): Response<Location>

    @GET("location/{id}")
    suspend fun getLocationById(
        @Path("id") idLocation: Int
    ): Response<Location>

    @GET("character/{ids}")
    suspend fun getPersonajesByIds(
        @Path("ids") ids: String
    ): Response<List<Personaje>>

    @GET("episode/{id}")
    suspend fun getEpisodeById(
        @Path("id") idLocation: Int
    ): Response<Episode>

}