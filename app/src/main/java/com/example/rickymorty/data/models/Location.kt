package com.example.rickymorty.data.models


import kotlinx.parcelize.Parcelize
import android.os.Parcelable

@Parcelize
data class Location(
    val created: String?,
    val dimension: String?,
    val id: Int?,
    val name: String?,
    val residents: List<String?>?,
    val type: String?,
    val url: String?
) : Parcelable